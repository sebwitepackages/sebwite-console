<?php
/**
 * Part of the $author$ PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Console\IO;

use Symfony\Component\Console\Input\ArgvInput as BaseArgvInput;

class ArgvInput extends BaseArgvInput
{

}
