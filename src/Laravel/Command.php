<?php
namespace Sebwite\Console\Laravel;

use Sebwite\Support\Vendor;

/**
 * The abstract Command class. Other commands can extend this class to benefit from a larger toolset
 *
 * @package     Sebwite\Console
 * @author      Robin Radic
 * @license     MIT
 * @copyright   2011-2015, Robin Radic
 * @link        http://radic.mit-license.org
 *
 * @deprecated  use Sebwite\Console\Command instead
 */
abstract class Command extends \Sebwite\Console\Command
{
}
