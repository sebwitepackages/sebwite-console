<?php
/**
 * Part of the $author$ PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Console\Helpers;

use Symfony\Component\Console\Helper\Helper as BaseHelper;

abstract class Helper extends BaseHelper implements HelperInterface
{

}
