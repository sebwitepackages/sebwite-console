<?php
/**
 * Part of the CLI PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */

namespace Sebwite\Console\Helpers;


interface HelperInterface
{
    public static function supported();
}
